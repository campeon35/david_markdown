# README

***Hoja03_Markdown_02***

![](Capturas/Ejercicio.PNG "Ejercicio propuesto por Ivan Lorenzo")

### Necesitaremos

* GitLab
* GitBash
* Sublime

***
## Punto e)

**Los cambios se suben con el comando "git push origin master"**  
![](Capturas/1.PNG "Subiendo el archivo")

## Punto f)

**Creamos el archivo y la carpeta de forma gráfica**  
![](Capturas/2.PNG "Creando los archivos")

*Luego entramos al git bash y vemos que aparecen listos para subirles al repositorio*  
![](Capturas/3.PNG "Comprobaciones")

## Punto g)

***Para que git ignore los archivos creamos el fichero .gitignore y escribimos dentro de él los nombres de los archivos***  
![](Capturas/4.PNG "Creando fichero que ignore")

*Y luego volvemos al bash a comprobar*  
![](Capturas/5.PNG "Comprobacion 2")

**Nos damos cuenta que desaparencen los archivos que pusimos en el gitignore pero se añade el propio archivo. Eso es para que los otros usuarios lo descarguen y también ignoren esos archivos**  
![](Capturas/6.PNG "Subiendo .gitignore")

## Punto i)

**Creamos el fichero y le introducimos el listado de los modulos**  
![](Capturas/7.PNG "Archivo david.md")

## Punto j)

**Para crear el tag usamos el comando "git tag" y le damos un nombre. En este caso v0.1**  
![](Capturas/8.PNG "Creando tag")

*Si queremos asegurarnos de que el tag se ha creado usamos el comando "git log"*  
![](Capturas/9.PNG "Comprobacion 3")

## Punto k)

***Debemos añadir los archivos al indice con el comando "git add"***  
![](Capturas/10.PNG "Archivo david.md")

**Luego hacer un "commit" y añadirle el parametro -m para añadir un mensaje y finalmente usar el comando "push" para subir todo al repositorio**  
![](Capturas/11.PNG "Archivo david.md")

***
# Tabla 

|  Compañero  | Enlace Repositorio |
|   :-----:   |   :------------:   |
|  Pablo Val  |      [Git][1]      |
|             | [Compañero2][2]    |

[1]: https://gitlab.com/PabloVal/pablo_markdown 
[2]: http://google.es  

***
***Hoja03_Markdown_03***

1.	Creacion de ramas
	*	Crear la rama
	*	Posicionarse en ella

**Para crear la rama utilizamos el comando "git branch" y para posicionarse "git checkout"**  
![](Capturas/12.PNG "Crear la rama y situarse en ella")

2.	Añadir un fichero y crea la rama remota
	*	Crea el fichero daw.md
	*	Haz el commit
	*	Sube los cambios a la rama

**Creamos el archivo y le añadimos la cabecera**  
![](Capturas/13.PNG "")

**Ahora le añadimos con "git add" y hacemos el "commit"**  
![](Capturas/14.PNG "")

**Y finalmente hacemos el push a la rama**  
![](Capturas/15.PNG "")

3.	Merge directo
	*	Posicionarse en la master
	*	Merge de las ramas

**Nos posicionamos en la rama Master y juntamos la rama "david" a la master**  
![](Capturas/16.PNG "")    
![](Capturas/17.PNG "")

4.	Haz un merge conflicto  
	*	Añade al fichero daw.md la tabla de 1º
	*	Añade el archivo y haz un commit	
	*	Añade al fichero daw.md la tabla de 2º
	*	Añade el archivo y haz un commit
	*	Haz el "merge" desde master

**Añadimos la tabla al fichero**  
![](Capturas/18.PNG "")

**Hacemos "add" y "commit"**  
![](Capturas/19.PNG "")

**Cambiamos de rama y hacemos lo mismo**  
![](Capturas/20.PNG "")  
![](Capturas/21.PNG "")  
![](Capturas/22.PNG "")

**Finalmente hacemos el merge y esperamos el fallo**  
![](Capturas/23.PNG "")  
![](Capturas/24.PNG "")

5.	Arreglo del conflicto
	*	Resuelve el conflicto

**Resolvemos el conflicto cambiando el fichero a mano**  
![](Capturas/25.PNG "")

6.	Tag y borrar rama
	*	Crea el tag v0.2
	*	Borra la rama david

**Creamos el tag**  
![](Capturas/26.PNG "")

**Y borramos la rama**  
![](Capturas/27.PNG "")

***Por ultimo subimos todo***